const User = require('../models/User');
const authenticationService = require('../services/authentication-service');

exports.getUsers = async function (res) {
    try {
        const users = await User.find({});
        return res.json(users);
    } catch (e) {
        return res.send({status: 'fail', message: e});
    }
};

exports.getUserById = async function (id, res) {
    if (id) {
        try {
            const user = await User.findOne({_id: id});
            return res.json(user);
        } catch (e) {
            return res.status(500).send({
                message: e
            });
        }
    } else {
        return res.status(400).send({
            message: 'No ID specified.'
        });
    }
};

exports.updateUser = async function (body, id, res) {
    if (id) {
        try {
            const userToUpdate = await User.findOne({_id: id});
            if (userToUpdate) {
                delete body._id;
                await User.findOneAndUpdate({_id: id}, body);
                const updatedUser = await User.findOne({_id: id});
                return res.json(updatedUser);
            } else {
                return res.status(404).send({
                    message: 'User not found.'
                });
            }
        } catch (e) {
            return res.status(500).send({
                message: e
            });
        }
    } else {
        return res.status(400).send({
            message: 'No ID specified.'
        });
    }
};

exports.deleteUser = async function (id, res) {
    if (id) {
        try {
            const userToDelete = await User.findOne({_id: id});
            if (userToDelete) {
                await userToDelete.delete();
                return res.sendStatus(200);
            } else {
                return res.status(404).send({
                    message: 'User not found.'
                });
            }
        } catch (e) {
            return res.status(500).send({
                message: e
            });
        }
    } else {
        return res.status(400).send({
            message: 'No ID specified.'
        });
    }
};

exports.createUser = async function createUser(body, res) {
    if (body) {
        try {
            const email = body.email;
            const exist = await User.findOne({email});
            if (exist) {
                return res.status(400).json({message: 'User already exists'});
            } else {
                body.password = await authenticationService.encryptPassword(body.password);
                const new_user = new User(body);
                const savedUser = await new_user.save();
                savedUser.token = await authenticationService.generateToken(savedUser);
                return res.json(savedUser);
            }
        } catch (e) {
            return res.status(500).send({
                message: e
            });
        }
    } else {
        return res.status(400).send({
            message: 'No body specified.'
        });
    }
};
