const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../models/User');

exports.register = async (body, res) => {
    const user = body;
    try {
        const email = user.email;
        const exist = await User.findOne({email});
        if (exist) {
            return res.status(400).json({message: 'User already exists'});
        } else {
            user.password = await exports.encryptPassword(user.password);
            const new_user = new User(user);
            const savedUser = await new_user.save();
            savedUser.token = await exports.generateToken(savedUser);
            return res.json(savedUser);
        }
    } catch (e) {
        return res.status(500).json({message: e});
    }
};

exports.login = async (body, res) => {
    try {
        const {email, password} = body;
        const user = await User.findOne({email: email});
        if (user) {
            const isValid = await bcrypt.compareSync(password, user.password);
            if (isValid) {
                const token = await exports.generateToken(user);
                return res.status(200).json({user, token});
            } else {
                return res.status(500).json({message: 'Wrong password.'});
            }
        } else {
            return res.status(404).json({message: 'User not found.'});
        }
    } catch (e) {
        return res.status(404).json({message: `Error login.: ${e}`});
    }
};

exports.validateToken = (req, res, next) => {
    const token = req.header('x-token');
    if (!token) {
        return res.status(400).json({message: "Token was not sent"});
    }
    try {
        const {id, email, role} = jwt.verify(token, process.env.SECRET_TOKEN);
        req.id = id;
        req.email = email;
        req.role = role;
    } catch (error) {
        return res.status(400).json({message: "Invalid token"});
    }
    next();
};

exports.validateAdminToken = (req, res, next) => {
    const token = req.header('x-token');
    if (!token) {
        return res.status(400).json({message: "Token was not sent"});
    }
    try {
        const {id, email, role} = jwt.verify(token, process.env.SECRET_TOKEN);
        req.id = id;
        req.email = email;
        req.role = role;
        if (role === 'ADMIN') {
            next();
        } else {
            return res.status(401).json({message: "Not authorized."});
        }
    } catch (error) {
        return res.status(400).json({message: "Invalid token"});
    }
};

exports.validateManagerToken = (req, res, next) => {
    const token = req.header('x-token');
    if (!token) {
        return res.status(400).json({message: "Token was not sent"});
    }
    try {
        const {id, email, role} = jwt.verify(token, process.env.SECRET_TOKEN);
        req.id = id;
        req.email = email;
        req.role = role;
        if (role === 'MANAGER' || role === 'ADMIN') {
            next();
        } else {
            return res.status(401).json({message: "Not authorized."});
        }
    } catch (error) {
        return res.status(400).json({message: "Invalid token"});
    }
};

exports.validateUserToken = (req, res, next) => {
    const token = req.header('x-token');
    if (!token) {
        return res.status(400).json({message: "Token was not sent"});
    }
    try {
        const {id, email, role} = jwt.verify(token, process.env.SECRET_TOKEN);
        req.id = id;
        req.email = email;
        req.role = role;
        if (role === 'USER' || role === 'MANAGER' || role === 'ADMIN') {
            next();
        } else {
            return res.status(401).json({message: "Not authorized."});
        }
    } catch (error) {
        return res.status(400).json({message: "Invalid token"});
    }
};

exports.encryptPassword = async (password) => {
    if (password) {
        const salt = bcrypt.genSaltSync();
        return bcrypt.hashSync(password, salt);
    } else {
        return 'Error password';
    }
};

exports.generateToken = (user) => {
    return new Promise((resolve, reject) => {
        if (user && user._id && user.email && user.role) {
            const payload = {id: user._id, email: user.email, role: user.role};
            jwt.sign(payload, process.env.SECRET_TOKEN, {expiresIn: '48h'}, (err, token) => {
                if (err) {
                    reject('There was an error during token generation.');
                } else {
                    resolve(token);
                }
            });
        } else {
            reject('There was an error during token generation.');
        }
    });
};
