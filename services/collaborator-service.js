const Collaborator = require('../models/Collaborator');

exports.getCollaborators = async function (res) {
	try {
		const collabs = await Collaborator.find({});
		return res.json(collabs);
	} catch (e) {
		return res.send({ status: 'fail', message: e });
	}
};

exports.getCollaboratorById = async function (id, res) {
	if (id) {
		try {
			const collab = await Collaborator.findOne({ _id: id });
			return res.json(collab);
		} catch (e) {
			return res.send({ status: 'fail', message: e });
		}
	} else {
		return res.send({ status: 'fail', message: 'No ID specified.' })
	}
};
exports.createCollaborator = async (body, res) => {
	const collab = body;
	try {
		const new_collab = new Collaborator(collab);
		const savedCollab = await new_collab.save();
		return res.json(savedCollab);

	} catch (e) {
		return res.status(400).json({ message: e });
	}
};

exports.updateCollaborator = async function (body, id, res) {
	if (id) {
		try {
			const collabToUpdate = await Collaborator.findOne({ _id: id });
			if (collabToUpdate) {
				delete body._id;
				await Collaborator.findOneAndUpdate({ _id: id }, body);
				const updatedCollab = await Collaborator.findOne({ _id: id });
				return res.json(updatedCollab);
			} else {
				return res.status(404).send({
					message: 'Collaborator not found.'
				});
			}
		} catch (e) {
			return res.status(500).send({
				message: e
			});
		}
	} else {
		return res.status(400).send({
			message: 'No ID specified.'
		});
	}
};

exports.deleteCollaborator = async function (id, res) {
	if (id) {
		try {
			const collabToDelete = await Collaborator.findOne({ _id: id });
			if (collabToDelete) {
				await collabToDelete.delete();
				return res.sendStatus(200);
			} else {
				return res.send({ status: 'fail', message: 'collaborator ' + id + 'not found.' })
			}
		} catch (e) {
			return res.send({ status: 'fail', message: e });
		}
	} else {
		return res.send({ status: 'fail', message: 'No id specified.' })
	}
};
