const faker = require('faker');
const Collaborator = require('../models/Collaborator');
const User = require('../models/User');
const authService = require('../services/authentication-service');

exports.populateUsers = async function (res) {
    try {
        const corentin = new User();
        corentin.firstName = 'Corentin';
        corentin.lastName = 'Bonneau';
        corentin.phone = _generatePhoneNumber();
        corentin.email = 'c.bonneau16@gmail.com';
        corentin.role = 'ADMIN';
        corentin.password = await authService.encryptPassword('123');
        await corentin.save();

        const guillaume = new User();
        guillaume.firstName = 'Guillaume';
        guillaume.lastName = 'Bon';
        guillaume.phone = _generatePhoneNumber();
        guillaume.email = 'guillaumebon16@gmail.com';
        guillaume.role = 'ADMIN';
        guillaume.password = await authService.encryptPassword('123');
        await guillaume.save();

        for (let i = 0; i < 100; i++) {
            const user = new User();
            user.firstName = faker.name.firstName();
            user.lastName = faker.name.lastName();
            user.phone = _generatePhoneNumber();
            user.email = faker.internet.email().toLowerCase();
            user.role = _randomRole();
            user.password = await authService.encryptPassword('123');
            await user.save();
        }
        return true;
    } catch (e) {
        return res.status(500).json({message: e});
    }
};

exports.populateCollaborators = async function (res) {
    try {
        for (let i = 0; i < 100; i++) {
            const collab = new Collaborator();
            collab.siret = _generateSiret();
            collab.companyName = faker.company.companyName();
            const TYPE = ["CUSTOMER", "MANUFACTURER"];
            collab.type = TYPE[Math.floor(Math.random() * 2)];
            collab.longitude = faker.address.longitude();
            collab.latitude = faker.address.latitude();
            await collab.save();
        }
        return true;
    } catch (e) {
        return res.status(500).json({message: e});
    }
};

function _generatePhoneNumber() {
    let number = '06';
    for (let i = 0; i < 8; i++) {
        number += Math.floor(Math.random() * 10);
    }
    return number;
}

function _randomRole() {
    const ROLES = ['ADMIN', 'MANAGER', 'SIMPLE'];
    return ROLES[Math.floor(Math.random() * 3)];
}

function _generateSiret() {
    let number = '00';
    for (let i = 0; i < 8; i++) {
        number += Math.floor(Math.random() * 10);
    }
    return number;
}
