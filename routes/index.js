const express = require('express');
const router = express.Router();

router.get('*', function (req, response, next) {
    response.send("HELLO WORLD");
});

module.exports = router;
