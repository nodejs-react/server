const mongoose = require('mongoose');

const dbconnect = async () => {
    try {
        mongoose.connect(process.env.MONGO_CONNECTION_STRING, {
                useNewUrlParser: true,
                useUnifiedTopology: true
            },
            () => console.log("Mongo OK")
        );
    } catch (error) {
        throw new Error('Mongo KO');
    }
};

module.exports = {dbconnect};
