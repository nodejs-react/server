const express = require('express');
const router = express.Router();
const populateService = require('../services/populate-service');

router.get('/users', async (req, res) => {
    await populateService.populateUsers(res);
    return res.sendStatus(200);
});

router.get('/collaborators', async (req, res) => {
    await populateService.populateCollaborators(res);
    return res.sendStatus(200);
});

router.get('/all', async (req, res) => {
    await populateService.populateUsers(res);
    await populateService.populateCollaborators(res);
    return res.sendStatus(200);
});

module.exports = router;
