const express = require('express');
const router = express.Router();
const collaboratorService = require('../services/collaborator-service');
const { validateToken, validateAdminToken, validateManagerToken } = require('../services/authentication-service');

// Get all collaborators
router.get('/', [validateToken], async (req, res) => {
	return await collaboratorService.getCollaborators(res);
});

// Get collaborator by id
router.get('/:id', [validateToken], async (req, res) => {
	return await collaboratorService.getCollaboratorById(req.params.id, res);
});

// cree un collaborator
router.post('/', [validateManagerToken, validateAdminToken], async (req, res) => {
	return await collaboratorService.createCollaborator(req.body, res);
});
// Update an collaborator
router.put('/:id', [validateManagerToken, validateAdminToken], async (req, res) => {
	return await collaboratorService.updateCollaborator(req.body, req.params.id, res);
});

// Delete an collaborator
router.delete('/:id', [validateManagerToken, validateAdminToken], async (req, res) => {
	return await collaboratorService.deleteCollaborator(req.params.id, res);
});

module.exports = router;
