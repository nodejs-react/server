const express = require('express');
const router = express.Router();
const userService = require('../services/user-service');
const {validateAdminToken, validateToken} = require('../services/authentication-service');

// Get all users
router.get('/', [validateAdminToken], async (req, res) => {
    return await userService.getUsers(res);
});

// Get user by id
router.get('/:id', [validateAdminToken], async (req, res) => {
    return await userService.getUserById(req.params.id, res);
});

// Update an user
router.put('/:id', [validateAdminToken], async (req, res) => {
    return await userService.updateUser(req.body, req.params.id, res);
});

// Delete an user
router.delete('/:id', [validateAdminToken], async (req, res) => {
    return await userService.deleteUser(req.params.id, res);
});

// Create an user
router.post('/', [validateToken], async (req, res) => {
    return await userService.createUser(req.body, res);
});

module.exports = router;
