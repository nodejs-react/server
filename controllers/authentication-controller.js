const express = require('express');
const router = express.Router();
const authenticationService = require('../services/authentication-service');

router.post('/register', async (req, res) => {
    return await authenticationService.register(req.body, res);
});

router.post('/login', async (req, res) => {
    return await authenticationService.login(req.body, res);
});

module.exports = router;
