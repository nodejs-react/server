const createError = require('http-errors');
const express = require('express');
const logger = require('morgan');
const cors = require('cors');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const authController = require('./controllers/authentication-controller');
const userController = require('./controllers/user-controller');
const populateController = require('./controllers/populate-controller');
const collaboratorController = require('./controllers/collaborator-controller');
const dbconnect = require('./core/config');
const dotenv = require('dotenv');

const app = express();
dotenv.config();
dbconnect.dbconnect();

// Middlewares
app.use(cors());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(logger('dev'));
app.use(helmet());
app.use(express.json());

// Routes
app.use('/api/v1/auth', authController);
app.use('/api/v1/collaborators', collaboratorController);
app.use('/api/v1/users', userController);
app.use('/api/v1/populate', populateController);

// Catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// Error handler
app.use(function (err, req, res, next) {
    // Set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // Render the error page
    res.status(err.status || 500);
    res.send('error');
});

app.listen(3000, function () {
    console.log('Listening on 3000.')
});

module.exports = app;
