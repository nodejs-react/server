const {Schema, model} = require('mongoose');

const UserModel = Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: [true, 'The email is not valid'],
        validate: [function (email) {
            const emailRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            return emailRegex.test(email); // Assuming email has a text attribute
        }, 'The email is not valid.'],
    },
    phone: {
        type: String,
        validate: [function (v) {
            return /\d{10}/.test(v);
        }, props => `${props.value} is not a valid phone number!`],
        required: [true, 'User phone number required']
    },
    role: {
        type: String,
        enum: ['ADMIN', 'MANAGER', 'SIMPLE'],
        required: [true, 'Role required'],
        default: 'SIMPLE'
    }
});

module.exports = model('User', UserModel);
