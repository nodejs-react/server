const { Double } = require('mongodb');
const { Schema, model } = require('mongoose');

const CollaboratorModel = Schema({
	siret: {
		type: String
	},
	companyName: {
		type: String
	},
	latitude: {
		type: Number,
		required: true,
	},
	longitude: {
		type: Number,
		required: true,
	},
	type: {
		type: String,
		enum: ["CUSTOMER", "MANUFACTURER"]
	}
});

module.exports = model('Collaborator', CollaboratorModel);
